<?php

declare(strict_types=1);

namespace SlyFoxCreative\MagentoKuebix\Model\Carrier;

use Magento\Directory\Model\RegionFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory;
use Magento\Quote\Model\Quote\Address\RateResult\MethodFactory;
use Magento\Shipping\Model\Carrier\AbstractCarrier;
use Magento\Shipping\Model\Carrier\CarrierInterface;
use Magento\Shipping\Model\Rate\Result;
use Magento\Shipping\Model\Rate\ResultFactory;
use Psr\Log\LoggerInterface;
use SlyFoxCreative\Kuebix\Address;
use SlyFoxCreative\Kuebix\Client;
use SlyFoxCreative\Kuebix\Item;
use SlyFoxCreative\Kuebix\Request as KuebixRequest;

class Kuebix extends AbstractCarrier implements CarrierInterface
{
    protected $_code = 'kuebix';

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateResultFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $regionFactory;

    public function __construct(
        ScopeConfigInterface $scopeConfig,
        ErrorFactory $rateErrorFactory,
        LoggerInterface $logger,
        ResultFactory $rateResultFactory,
        MethodFactory $rateMethodFactory,
        RegionFactory $regionFactory,
        array $data = []
    ) {
        $this->rateResultFactory = $rateResultFactory;
        $this->rateMethodFactory = $rateMethodFactory;
        $this->regionFactory = $regionFactory;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    /**
     * @return array
     */
    public function getAllowedMethods()
    {
        return [$this->_code => $this->getConfigData('name')];
    }

    /**
     * @return bool|Result
     */
    public function collectRates(RateRequest $request)
    {
        if ($request->getPackageWeight() < $this->getConfigData('minimum_weight')) {
            return false;
        }

        $client = new Client(
            $this->getConfigData('kuebix_client_id'),
            $this->getConfigData('username'),
            $this->getConfigData('api_key'),
            $this->getConfigData('endpoint_url')
        );

        $origStreet = $request->getOrigStreet() ?: '123 No Street';

        if ($request->getOrigCity()) {
            $origCity = $request->getOrigCity();
        } else {
            $origCity = $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_CITY,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            );
        }

        if ($request->getOrigRegionCode()) {
            $origRegionCode = $request->getOrigRegionCode();
        } else {
            $origRegionCode = $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_REGION_ID,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            );
        }
        if (is_numeric($origRegionCode)) {
            $origRegionCode = $this->regionFactory->create()->load($origRegionCode)->getCode();
        }

        if ($request->getOrigPostcode()) {
            $origPostal = $request->getOrigPostcode();
        } else {
            $origPostal = $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_ZIP,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            );
        }

        if ($request->getOrigCountry()) {
            $origCountry = $request->getOrigCountry();
        } else {
            $origCountry = $this->_scopeConfig->getValue(
                \Magento\Sales\Model\Order\Shipment::XML_PATH_STORE_COUNTRY_ID,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                $request->getStoreId()
            );
        }

        $origin = new Address(
            $origStreet,
            $origCity,
            $origRegionCode,
            $origPostal,
            $origCountry
        );

        $destStreet = $request->getDestStreet() ?: '123 No Street';
        $destCity = $request->getDestCity() ?: 'No City';

        $destination = new Address(
            $destStreet,
            $destCity,
            $request->getDestRegionCode(),
            $request->getDestPostcode(),
            $request->getDestCountryId()
        );

        $items = $this->getItems($request);
        $options = [];

        if ($items['overlength']) {
            $options[] = 'extremeLength';
        }

        if ($request->getPackageWeight() < $this->getConfigData('single_shipment_ceiling')) {
            $options[] = 'singleShipment';
        }

        $kuebixRequest = new KuebixRequest(
            $client,
            $origin,
            $destination,
            $origin,
            $items['lineItems'],
            $options
        );

        if ($this->getConfigData('debug')) {
            $this->_logger->log(100, print_r($kuebixRequest->json(), true));
        }

        $response = $client->request($kuebixRequest);

        $result = $this->rateResultFactory->create();

        $method = $this->rateMethodFactory->create();

        $method->setCarrier($this->_code);
        // Get the title from the configuration, as defined in system.xml
        $method->setCarrierTitle($this->getConfigData('title'));

        $method->setMethod($this->_code);
        // Get the title from the configuration, as defined in system.xml
        $method->setMethodTitle($this->getConfigData('name'));

        $cost = $response->minimumCost();

        $price = ceil($cost) + $this->getConfigData('handling') + ($items['quantity'] * $this->getConfigData('handling_per_item'));

        $method->setCost($cost);
        $method->setPrice($price);

        $result->append($method);

        return $result;
    }

    public function isCityRequired()
    {
        return true;
    }

    public function isZipCodeRequired($countryId = null)
    {
        return true;
    }

    private function getItems(RateRequest $request)
    {
        $quantity = 0;
        $overlength = false;
        $lineItems = [];
        $requestItems = $request->getAllItems();

        foreach ($requestItems as $item) {
            $quantity += $item->getQty();

            $product = $item->getProduct();
            $product->load($item->getProduct()->getId());

            $dimensions = [$product->getLength(), $product->getWidth(), $product->getHeight()];
            foreach ($dimensions as $dimension) {
                if ($dimension >= $this->getConfigData('overlength')) {
                    $overlength = true;
                }
            }

            $lineItems[] = new Item(
                (string) $item->getSku(),
                (string) $product->getAttributeText('freight_class'),
                (string) $product->getAttributeText('nmfc'),
                (int) $product->getLength(),
                (int) $product->getWidth(),
                (int) $product->getHeight(),
                (float) $item->getWeight(),
                (int) $item->getQty()
            );
        }

        return ['quantity' => $quantity, 'overlength' => $overlength, 'lineItems' => $lineItems];
    }
}
