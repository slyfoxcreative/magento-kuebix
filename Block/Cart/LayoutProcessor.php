<?php

namespace SlyFoxCreative\MagentoKuebix\Block\Cart;

use Magento\Checkout\Block\Cart\LayoutProcessor as MagentoLayoutProcessor;

class LayoutProcessor extends MagentoLayoutProcessor
{
    protected function isCityActive()
    {
        return true;
    }

    protected function isStateActive()
    {
        return true;
    }
}
